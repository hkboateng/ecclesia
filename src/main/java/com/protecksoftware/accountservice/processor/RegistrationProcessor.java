package com.protecksoftware.accountservice.processor;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.businessobjects.RegistrationRequest;
import com.protecksoftware.accountservice.businessobjects.RegistrationResponse;
import com.protecksoftware.accountservice.domain.Email;
import com.protecksoftware.accountservice.service.RegistrationService;

@Component
public class RegistrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(RegistrationProcessor.class.getName());
	
	@Autowired
	private RegistrationService registrationServiceImpl;
	
	private RegistrationProcessor() {
		super();
	}

	public RegistrationResponse registerBusiness(RegistrationRequest request){
		RegistrationResponse response = null;
		AuthenticationResponse authenticationResponse = null;
		boolean email = registrationServiceImpl.isUserUnique(request.getEmailAddress(),request.getPhoneNumber());
		if(!email){
			response = new RegistrationResponse();
			response.setResponseStatus(false);
			response.setMessage("Email Address or Phone Number already exist is our system. Login or Reset your password");
			logger.info("Email Address:"+request.getEmailAddress()+" or Phone Number: "+request.getPhoneNumber()+" already exist is our system. Login or Reset your password");
			return response;
		}
		
		response = registrationServiceImpl.registerAccount(request);
		if(response.getResponseStatus()){
			request.setAccountId(response.getAccountId());
			authenticationResponse = saveLoginCredentials(request);
			if(authenticationResponse.isResult()){
				logger.info("Business :"+response.getCompany().getBusinessName()+" has being created successfully.. Account Number:"+response.getCompany().getAccountNumber());
			}
		}
		return response;
	}

	public RegistrationResponse registerIndividual(RegistrationRequest request){
		RegistrationResponse response = null;
		AuthenticationResponse authenticationResponse = null;
		boolean email = registrationServiceImpl.isUserUnique(request.getEmailAddress(),request.getPhoneNumber());
		if(!email){
			response = new RegistrationResponse();
			response.setResponseStatus(false);
			response.setMessage("Email Address or Phone Number already exist is our system. Login or Reset your password");
			logger.info("Email Address:"+request.getEmailAddress()+" or Phone Number: "+request.getPhoneNumber()+" already exist is our system. Login or Reset your password");
			return response;
		}
		
		response = registrationServiceImpl.registerAccount(request);
		if(response.getResponseStatus()){
			request.setAccountId(response.getAccountId());
			authenticationResponse = saveLoginCredentials(request);
			if(authenticationResponse.isResult()){
				logger.info("Business :"+response.getAccountId()+" has being created successfully.. Account Number:"+response.getCompany().getAccountNumber());
			}
		}
		return response;
	}
	
	public List<String> validation(RegistrationRequest request){
		List<String> validationErrors =  new ArrayList<String>();;
		if(request == null){
			validationErrors.add("Registration Request is null. Cannot proceed with validation.");
			logger.error("Registration Request is null. Cannot proceed with validation.");
		}else{
			if(request.getUserRole() < 9){
				validationErrors.add("Role Id:"+request.getUserRole()+" is invalid");
			}
		}
		return validationErrors;
	}
	
	public AuthenticationResponse saveLoginCredentials(RegistrationRequest request ){

		
		AuthenticationResponse authenticationResponse = null;
		try {
			authenticationResponse = registrationServiceImpl.saveLoginInformation(request);
		} catch ( Exception e) {
			authenticationResponse = new AuthenticationResponse();
			authenticationResponse.setResult(false);
			authenticationResponse.setMessage("Ecclesia could not save new User Login information...Check the  statck trace for information.");
			logger.warn(e.getMessage(),e);
		}finally{		
		}
		
		return authenticationResponse;
	}
	

}
