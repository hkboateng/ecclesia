package com.protecksoftware.accountservice.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the userpermission database table.
 * 
 */
@Entity
@Table(name="userpermission")
@NamedQuery(name="Userpermission.findAll", query="SELECT u FROM Userpermission u")
public class Userpermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userpermissionId;

	private BigInteger companyId;

	private int permission;

	private BigInteger userId;

	public Userpermission() {
	}

	public int getUserpermissionId() {
		return this.userpermissionId;
	}

	public void setUserpermissionId(int userpermissionId) {
		this.userpermissionId = userpermissionId;
	}

	public BigInteger getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(BigInteger companyId) {
		this.companyId = companyId;
	}

	public int getPermission() {
		return this.permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}