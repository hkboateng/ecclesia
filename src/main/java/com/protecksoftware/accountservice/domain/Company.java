package com.protecksoftware.accountservice.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Company database table.
 * 
 */
@Entity
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company implements Serializable,Customer {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int companyId;

	private String companyName;

	private String companyName2;

	//bi-directional one-to-one association to Account
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="companyId")
	private Account account;

	//bi-directional one-to-one association to Email
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="emailId")
	private Email email;

	//bi-directional one-to-one association to Phone
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="phoneId")
	private Phone phone;

	//bi-directional one-to-one association to Phone
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="addressId")
	private Address address;
	
	//bi-directional one-to-one association to ContactPerson
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="contactPersonId")
	private ContactPerson contactPerson;

	public Company() {
	}

	public int getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Address getAddressId() {
		return this.address;
	}

	public void setAddressId(Address addressId) {
		this.address = addressId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName2() {
		return this.companyName2;
	}

	public void setCompanyName2(String companyName2) {
		this.companyName2 = companyName2;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Phone getPhone() {
		return this.phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public ContactPerson getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Override
	public String getAccountNumber() {
		return getAccount().getAccountNumber();
	}

	@Override
	public String getAccountType() {
		return "company";
	}

	@Override
	public int getAccountId() {
		return getAccount().getAccountId();
	}


}