package com.protecksoftware.accountservice.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Individual database table.
 * 
 */
@Entity
@NamedQuery(name="Individual.findAll", query="SELECT i FROM Individual i")
public class Individual implements Serializable,Customer {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int individualId;

	private String firstname;

	private String lastname;

	private String middlename;

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="accountId")
	private Account account;

	//bi-directional one-to-one association to Address
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="addressId")
	private Address address;

	//bi-directional one-to-one association to Email
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="emailId")
	private Email email;

	//bi-directional one-to-one association to Phone
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="phoneId")
	private Phone phone;

	public Individual() {
	}

	public int getIndividualId() {
		return this.individualId;
	}

	public void setIndividualId(int individualId) {
		this.individualId = individualId;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Phone getPhone() {
		return this.phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	@Override
	public String getAccountNumber() {
		return getAccount().getAccountNumber();
	}

	@Override
	public String getAccountType() {
		return "individual";
	}

	@Override
	public int getAccountId() {
		return getAccount().getAccountId();
	}
	
	

}