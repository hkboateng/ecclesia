package com.protecksoftware.accountservice.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ContactPerson database table.
 * 
 */
@Entity
@NamedQuery(name="ContactPerson.findAll", query="SELECT c FROM ContactPerson c")
public class ContactPerson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int contactId;

	private String email;

	private String firstname;

	private String lastname;

	private String middlename;

	private String phoneNumber;

	public ContactPerson() {
	}

	public int getContactId() {
		return this.contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}