package com.protecksoftware.accountservice.domain.dto;

import com.protecksoftware.accountservice.domain.Company;

public class CompanyDTO {
	private int accountId;
	
	private String businessName;
	
	private String emailAddress;
	
	private String phoneNumber;
	
	private String accountNumber;
	
	public CompanyDTO(Company company) {
		this.accountId = company.getAccount().getAccountId();
		this.businessName = company.getCompanyName();
		this.emailAddress = company.getEmail().getEmailAddress();
		this.phoneNumber = company.getPhone().getPhoneNumber();
		this.accountNumber = company.getAccountNumber();
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	

}
