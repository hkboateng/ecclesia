package com.protecksoftware.accountservice.domain.dto;

import com.protecksoftware.accountservice.domain.Individual;

public class IndividualDTO {
	
	private int accountId;
	
	private int customerId;
	
	private String emailAddress;
	
	private String emailType;
	
	private String phoneNumber;
	
	private String phoneType;
	
	public IndividualDTO(Individual individual) {
		this.accountId = individual.getAccountId();
		this.customerId = individual.getIndividualId();
		this.emailAddress = individual.getEmail().getEmailAddress();
		this.emailType = individual.getEmail().getEmailType();
		this.phoneNumber = individual.getPhone().getPhoneNumber();
		this.phoneType = individual.getPhone().getPhoneType();
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	
}
