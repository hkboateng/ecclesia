package com.protecksoftware.accountservice.domain;

import java.util.List;

public final class AuthenticationUser {
	private final User user;
	
	private final List<String> permissionList;
	
	Customer customer = null;
	
	public AuthenticationUser(User user, List<String> permissionList) {
		super();
		this.user = user;
		this.permissionList = permissionList;
	}

	public User getUser() {
		return user;
	}

	public List<String> getPermissionList() {
		return permissionList;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	
}
