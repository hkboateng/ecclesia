package com.protecksoftware.accountservice.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface Customer {
	String getAccountNumber();
	
	String getAccountType();
	
	int getAccountId();
}
