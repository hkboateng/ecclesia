package com.protecksoftware.accountservice.resource;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.protecksoftware.accountservice.businessobjects.RegistrationRequest;
import com.protecksoftware.accountservice.businessobjects.RegistrationResponse;
import com.protecksoftware.accountservice.processor.RegistrationProcessor;


/**
 * Resource for Account serving on non-mobile app
 * @author abankwah
 *
 */
@RestController
@RequestMapping(value="/register")
public class RegistrationResource {
	private static final Logger logger = LoggerFactory.getLogger(RegistrationResource.class.getName());

	@Autowired
	private RegistrationProcessor registrationProcessor;

	private @Value("${sharedSecret}") String accessKey;
	
	@RequestMapping(value="/authentication/test", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody String test(){
		String response = "working";

		
		return response;
	}
	@RequestMapping(value="/business", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody RegistrationResponse register(HttpServletRequest request){

		RegistrationRequest registrationRequest = null;
		RegistrationResponse registrationResponse = null;
		try{
			registrationRequest = new RegistrationRequest(request,accessKey,"business");
			List<String> validation = registrationProcessor.validation(registrationRequest);
			if(!validation.isEmpty()){
				registrationResponse = new RegistrationResponse();
				registrationResponse.setRegistrationErrors(validation);
				registrationResponse.setMessage("There are errors in the form.. please correct the errors below and try resubmit it again.");
				return registrationResponse;
			}
			registrationResponse = registrationProcessor.registerBusiness(registrationRequest);
		}catch(IOException e){
			registrationResponse = new RegistrationResponse();
			registrationResponse.setMessage("An Error occured while registering Business... Try again later.");
			logger.warn(e.getMessage(),e);
		}catch(Exception e){
			registrationResponse = new RegistrationResponse();
			registrationResponse.setMessage("An Error occured while registering Business... Try again later.");
			logger.warn(e.getMessage(),e);
			
		}
		return registrationResponse;
	}
	
	@RequestMapping(value="/individual", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody RegistrationResponse registerIndividual(HttpServletRequest request){

		RegistrationRequest registrationRequest = null;
		RegistrationResponse registrationResponse = null;
		try{
			registrationRequest = new RegistrationRequest(request,accessKey,"individual");
			List<String> validation = registrationProcessor.validation(registrationRequest);
			if(!validation.isEmpty()){
				registrationResponse = new RegistrationResponse();
				registrationResponse.setRegistrationErrors(validation);
				registrationResponse.setMessage("There are errors in the form.. please correct the errors below and try resubmit it again.");
				return registrationResponse;
			}
			registrationResponse = registrationProcessor.registerBusiness(registrationRequest);
		}catch(IOException e){
			registrationResponse = new RegistrationResponse();
			registrationResponse.setMessage("An Error occured while registering Business... Try again later.");
			logger.warn(e.getMessage(),e);
		}catch(Exception e){
			registrationResponse = new RegistrationResponse();
			registrationResponse.setMessage("An Error occured while registering Business... Try again later.");
			logger.warn(e.getMessage(),e);
			
		}
		

		
		return registrationResponse;
	}
	
}
