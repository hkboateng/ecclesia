package com.protecksoftware.accountservice.resource;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.protecksoftware.accountservice.authentication.AuthenticationRequest;
import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.domain.AuthenticationUser;
import com.protecksoftware.accountservice.domain.Customer;
import com.protecksoftware.accountservice.service.AccountService;
import com.protecksoftware.accountservice.service.AuthenticationService;
import com.protecksoftware.accountservice.utils.AccountServiceUtils;

@RestController
@RequestMapping(value="/auth")
public class AuthenticationResource {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationResource.class.getName());
	
	@Autowired
	private AuthenticationService authenticationServiceImpl;
	
	@Autowired
	private AccountService accountSvcImpl;

	@RequestMapping(value = "/authenticateUser", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public String home(@RequestBody String request) {
		String result = null;
		AuthenticationResponse response = null;
		String authenticationRequestString = new String(Base64.decodeBase64(request));
		AuthenticationRequest authenticationRequest = null;
		try{
			authenticationRequest = AccountServiceUtils.convertFromJSON(AuthenticationRequest.class, authenticationRequestString);
		}catch(IOException e){
			response = new AuthenticationResponse(false);
			response.setMessage("Authentication Request Object is null or cannot be processed.");
		}
		String username = authenticationRequest.getUsername().trim();
		String password = authenticationRequest.getPassword();
		boolean authenticationResult = false;
		
		String[] permissions = null;
		
		logger.info("User: "+username+" is being authenticated right..Welcome to Authentication Service.");
		if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)){
			AuthenticationUser authenticationUser = null;
			try {
				
				authenticationUser = authenticationServiceImpl.findCustomerByUsername(username);				
				if(authenticationUser != null){
					if(authenticationUser.getUser().isAccountLocked()){
						response = new AuthenticationResponse();
						result = "User with username: "+username+" account is locked.";
						logger.info(result);
						authenticationResult = false;
						response.setMessage(result);
						response.setUserId(0);						
					}else if(authenticationUser.getUser().isAccountExpired()){
						response = new AuthenticationResponse();
						result = "User with username: "+username+" account is expired.";
						logger.info(result);
						authenticationResult = false;
						response.setMessage(result);
						response.setUserId(0);	
					}else{
						response = authenticationServiceImpl.validateCustomerCredentials(authenticationUser,password);
						if(response.isResult()){
							
							response.setMessage("Authentication valid");
							Customer customer = accountSvcImpl.findCustomerByAccountId(authenticationUser.getUser().getAccountId());
							response.setCustomer(customer);
							response.setUserId(authenticationUser.getUser().getUserId());
							
							permissions = AccountServiceUtils.convertListToArray(authenticationUser.getPermissionList());
							response.setPermissions(permissions);
						}
						authenticationResult = response.isResult();
					}
				
				}else{
					response = new AuthenticationResponse();
					result = "User with username: "+username+" is invalid.";
					logger.info(result);
					authenticationResult = false;
					response.setMessage(result);
					response.setUserId(0);
				}
				response.setResult(authenticationResult);
				result = AccountServiceUtils.convertToJSON(response);
				logger.info(result);
			} catch (JsonProcessingException e) {
				logger.warn(e.getMessage(),e);	
			} catch (IOException e) {
				logger.warn(e.getMessage(),e);	
			}finally{
				if((response.isResult() && response.getUserId() > 0)){
					authenticationServiceImpl.saveLoginTime(authenticationUser.getUser().getUsername(),authenticationUser.getUser().getAccountId(),response.isResult());
				}
				permissions = null;
			}
		}

		return result;
	}
	
	@RequestMapping(value="/authentication/test", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody String test(){
		String response = "working";

		
		return response;
	}
}
