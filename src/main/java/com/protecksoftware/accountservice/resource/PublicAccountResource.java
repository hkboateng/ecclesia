package com.protecksoftware.accountservice.resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/public")
public class PublicAccountResource {

	@RequestMapping(value="/register/{accountType}", method=RequestMethod.POST, consumes="application/json", produces="application/json")
	public String register(@PathVariable("accountType") String accountType){
		return accountType;
	}
	
	@RequestMapping(value="/register/test", method=RequestMethod.GET, produces="application/json")
	public String registerS(){
		return "accountType";
	}
}
