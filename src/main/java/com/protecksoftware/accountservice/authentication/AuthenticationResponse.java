package com.protecksoftware.accountservice.authentication;

import com.protecksoftware.accountservice.domain.Company;
import com.protecksoftware.accountservice.domain.Customer;
import com.protecksoftware.accountservice.domain.Individual;

public class AuthenticationResponse {


	private boolean result;

	private String message;

	private int userId;
	
	private long employeeId;
	
	private String[] permissions;
	
	private int loginAttempts;
	
	private Company company;
	
	private Individual individual;
	
	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	/**
	 * @param result
	 */
	public AuthenticationResponse(boolean result) {
		this.result = result;
	}
	
	public AuthenticationResponse() {
		//Default Constructor
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String[] getPermissions() {
		return permissions;
	}

	public void setPermissions(String[] permissions) {
		this.permissions = permissions;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public int getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Individual getIndividual() {
		return individual;
	}

	public void setIndividual(Individual individual) {
		this.individual = individual;
	}

	public void setCustomer(Customer customer) {
		if(customer.getAccountType().equalsIgnoreCase("individual")){
			this.individual = (Individual) customer;
		}else{
			this.company = (Company) customer;
		}
		
	}

	
}
