package com.protecksoftware.accountservice.service;

import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.businessobjects.RegistrationRequest;
import com.protecksoftware.accountservice.businessobjects.RegistrationResponse;
import com.protecksoftware.accountservice.domain.Email;

public interface RegistrationService {

	public RegistrationResponse registerAccount(RegistrationRequest request);
	
	public RegistrationResponse findAccountByAccountId(String accountId);
	
	public RegistrationResponse findAccountByAccountId(Integer accountId);

	AuthenticationResponse saveLoginInformation(RegistrationRequest request);

	boolean findEmailByEmailAddress(String emailAddress);

	boolean isUserUnique(String emailAddress, String phoneNumber);
}
