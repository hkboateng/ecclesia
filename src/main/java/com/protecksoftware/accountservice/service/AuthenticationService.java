package com.protecksoftware.accountservice.service;

import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.domain.AuthenticationUser;

public interface AuthenticationService {
	AuthenticationUser findCustomerByUsername(String username);
	
	AuthenticationResponse validateCustomerCredentials(AuthenticationUser user, String password);

	void saveLoginTime(String username, int accountId, Boolean result);
}
