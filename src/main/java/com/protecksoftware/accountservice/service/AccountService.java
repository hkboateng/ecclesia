package com.protecksoftware.accountservice.service;

import org.hibernate.Session;

import com.protecksoftware.accountservice.domain.Customer;

public interface AccountService {

	Customer findCustomerByAccountId(int accountId);

	Customer findIndividualCustomerByAccountId(int accountId,Session session);

	Customer findCompanyCustomerByAccountId(int accountId,Session session);
}
