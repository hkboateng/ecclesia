package com.protecksoftware.accountservice.service.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Statement;
import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.businessobjects.RegistrationRequest;
import com.protecksoftware.accountservice.businessobjects.RegistrationResponse;
import com.protecksoftware.accountservice.domain.Account;
import com.protecksoftware.accountservice.domain.Address;
import com.protecksoftware.accountservice.domain.Company;
import com.protecksoftware.accountservice.domain.ContactPerson;
import com.protecksoftware.accountservice.domain.Email;
import com.protecksoftware.accountservice.domain.Individual;
import com.protecksoftware.accountservice.domain.Phone;
import com.protecksoftware.accountservice.domain.Subscription;
import com.protecksoftware.accountservice.service.RegistrationService;
import com.protecksoftware.accountservice.utils.AccountServiceUtils;

@Component
public class RegistrationServiceImpl implements RegistrationService{

	private static final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class.getName());
	
	private @Value("${authentication.saveEmployeeLoginCredential}") String saveEmployeeLoginCredential;
	
	@Autowired
	private javax.sql.DataSource dataSource;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	  this.sessionFactory = sessionFactory;
	}
	public SessionFactory getSessionFactory() {
	  return sessionFactory;
	}

	
	public int saveCompany(RegistrationRequest request, Email email, ContactPerson person, Address address, Phone phone, Account account){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationResponse response = null;
		int key = 0;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into Company (companyName,companyName2,addressId,phoneId,emailId,contactPersonId,accountId) values (?,?,?,?,?,?,?);",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,request.getBusinessName());
			ps.setString(2,request.getBusinessName2());
			ps.setInt(3, address.getAddressId());
			ps.setInt(4, phone.getPhoneId());
			ps.setInt(5, email.getEmailId());
			ps.setInt(6, person.getContactId());
			ps.setInt(7, account.getAccountId());
			int Id = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(Id);
			}
		}catch(SQLException e){
			response = new AuthenticationResponse();
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with User Id: "+response.getUserId()+" and Username :"+request.getUsername());
		}catch(Exception e){
			response = new AuthenticationResponse();
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with User Id: "+response.getUserId()+" and Username :"+request.getUsername());
		}
		return key;
	}


	@Transactional
	@Override
	public RegistrationResponse registerAccount(RegistrationRequest request) {
		RegistrationResponse response = null;
		Session session = getSessionFactory().openSession();
		session.setFlushMode(FlushMode.MANUAL);
		Serializable i = null;
		if(request.getAccountType().equalsIgnoreCase("business") || request.getAccountType().equalsIgnoreCase("company")){
			try{
				
				Address address = new Address();
				address.setAddress1(request.getAddress1());
				address.setAddress2(request.getAddress2());
				address.setCity(request.getCity());
				address.setRegion(request.getRegion());		
				
				Phone phone = new Phone();
				phone.setCountrycode("+233");
				phone.setPhoneNumber(request.getPhoneNumber());
				phone.setPhoneType("mobile");//TODO - USer shd set this parameter
				phone.setPrimaryPhone(true);
				
				Email email = new Email();
				email.setEmailAddress(request.getEmailAddress());
				email.setEmailType("primary");

				ContactPerson person = new ContactPerson();
				person.setFirstname(request.getContactFirstName());
				person.setLastname(request.getContactLastName());
				Account account = new Account();
				account.setAccessKey(AccountServiceUtils.generateAccountKey());
				account.setSecretKey(request.getUserKey());
				account.setAccountType(request.getAccountType());
				account.setAccountNumber(AccountServiceUtils.generateAccountNumber());
				
				Subscription subscription = session.get(Subscription.class, 1);
				account.setSubscription(subscription);
				Company company = new Company();
				company.setAccount(account);
				company.setAddressId(address);
				company.setCompanyName(request.getBusinessName());
				company.setCompanyName2(request.getBusinessName2());
				company.setContactPerson(person);
				company.setPhone(phone);
				company.setEmail(email);
				session.save(company);
				session.flush();
				response = new RegistrationResponse();
				response.setAccountId(account.getAccountId());
				company.setAccount(account);
				response.setCompanyDTO(company);
				response.setResponseStatus(true);
				
			}catch(HibernateException e){
				response = new RegistrationResponse();
				response.setResponseStatus(false);
				logger.warn(e.getMessage(),e);	
			}
		}else if(request.getAccountType().equalsIgnoreCase("individual")){
			Address address = new Address();
			address.setAddress1(request.getAddress1());
			address.setAddress2(request.getAddress2());
			address.setCity(request.getCity());
			address.setRegion(request.getRegion());		
			
			Phone phone = new Phone();
			phone.setCountrycode("+233");
			phone.setPhoneNumber(request.getPhoneNumber());
			phone.setPhoneType("mobile");//TODO - USer shd set this parameter
			phone.setPrimaryPhone(true);
			
			Email email = new Email();
			email.setEmailAddress(request.getEmailAddress());
			email.setEmailType("primary");

			Account account = new Account();
			account.setAccessKey(AccountServiceUtils.generateAccountKey());
			account.setSecretKey(request.getUserKey());
			account.setAccountType(request.getAccountType());
			account.setAccountNumber(AccountServiceUtils.generateAccountNumber());
			Subscription subscription = session.get(Subscription.class, 1);
			account.setSubscription(subscription);
			
			
			Individual individual = new Individual();
			individual.setAccount(account);
			individual.setAddress(address);
			individual.setEmail(email);
			individual.setFirstname(request.getContactFirstName());
			individual.setLastname(request.getContactLastName());
			individual.setPhone(phone);
			individual.setMiddlename("");
			session.save(individual);
			session.flush();
			response = new RegistrationResponse();
			response.setAccountId(account.getAccountId());
			individual.setAccount(account);
			response.setIndividual(individual);
			response.setResponseStatus(true);
		}
		return response;
	}
	
	@Transactional
	@Override
	public RegistrationResponse findAccountByAccountId(String accountId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional
	@Override
	public RegistrationResponse findAccountByAccountId(Integer accountId) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Transactional
	@Override
	public boolean findEmailByEmailAddress(String emailAddress){
		int email = 0;
		try{
			Session session = getSessionFactory().openSession();
			email = session.createQuery("from Email e where e.emailAddress =:emailAddress")
			.setString("emailAddress", emailAddress)
			.list().size();
		}catch(HibernateException e){
			logger.warn(e.getMessage(),e);
		}
		return (email > 0) ? false : true;
	}
	
	@Transactional
	@Override
	public boolean isUserUnique(String emailAddress,String phoneNumber){
		int status = 0;
		boolean isUnique = true;
		try{
			Session session = getSessionFactory().openSession();
			if(isUnique){
				status = session.createQuery("from Email e where e.emailAddress =:emailAddress")
				.setString("emailAddress", emailAddress)
				.list()
				.size();
				if(status > 0){
					isUnique = false;
				}
			}

			if(isUnique){
				status = session.createQuery("from Phone p where p.phoneNumber =:phoneNumber")
				.setString("phoneNumber", phoneNumber)
				.list()
				.size();
				if(status > 0){
					isUnique = false;
				}
			}
		}catch(HibernateException e){
			logger.warn(e.getMessage(),e);
		}
		return isUnique;
	}

	@Override
	public AuthenticationResponse saveLoginInformation(RegistrationRequest request) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationResponse response = null;
		int key = 0;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into user (username,password,enabled,accessKey,accountId,lastLoggedIn,accountLocked, accountExpired) values (?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,request.getUsername());
			ps.setString(2,request.getRawPassword());
			ps.setBoolean(3,true);
			ps.setString(4, request.getUserKey());
			ps.setInt(5, request.getAccountId());
			ps.setLong(6,new Date().getTime());
			ps.setBoolean(7,false);
			ps.setBoolean(8, false);
			int Id = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(Id);
			}
			response = new AuthenticationResponse();
			int userId = rs.getInt(Id);
			response.setUserId(userId);
			addUserPermissions(request.getUserRole(),request.getAccountId(),userId,conn);
			conn.commit();
			response.setMessage("Employee has being saved successfully for AccountId: "+request.getAccountId());
		}catch(SQLException e){
			response = new AuthenticationResponse();
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with User Id: "+response.getUserId()+" and Username :"+request.getUsername());
		}catch(Exception e){
			response = new AuthenticationResponse();
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with User Id: "+response.getUserId()+" and Username :"+request.getUsername());
		}finally{
			request = null;
		}
		return response;
	}
	private AuthenticationResponse addUserPermissions(int roleId, int accountId, int userId, Connection conn) {
		AuthenticationResponse response = null;
		PreparedStatement ps = null;
		try{
			if(conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}
			ps = conn.prepareStatement("insert into userpermission (userpermissionId,permissionId,userId,accountId) values (NULL,?,?,?) ");
				ps.setInt(1,roleId);
				ps.setInt(2,userId);
				ps.setInt(3,accountId);	
				ps.executeUpdate();

			response = new AuthenticationResponse();
			response.setResult(true);
			logger.info("Account with Id: "+accountId+" and user Id: "+userId+" has security information saved successfully.");
		}catch(SQLException e){
			response = new AuthenticationResponse();
			response.setResult(false);
			response.setMessage("Error occured adding User Permissions.");
			logger.warn(e.getMessage(),e);			
		}catch(Exception e){
			response = new AuthenticationResponse();
			response.setResult(false);
			response.setMessage("Error occured adding User Permissions.");
			logger.warn(e.getMessage(),e);	
		}
		return response;
	}
	

}
