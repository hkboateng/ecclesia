package com.protecksoftware.accountservice.service.impl;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.protecksoftware.accountservice.domain.Account;
import com.protecksoftware.accountservice.domain.Company;
import com.protecksoftware.accountservice.domain.Customer;
import com.protecksoftware.accountservice.domain.Individual;
import com.protecksoftware.accountservice.service.AccountService;

@Component
public class AccountSvcImpl implements AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountSvcImpl.class.getName());
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	  this.sessionFactory = sessionFactory;
	}
	public SessionFactory getSessionFactory() {
	  return sessionFactory;
	}
	
	@Override
	@Transactional
	public Customer findCustomerByAccountId(int accountId) {
		Session session = getSessionFactory().openSession();
		Account account = session.get(Account.class,accountId);
		Customer customer = null;
		if(account != null && account.getAccountType().equals("individual")){
			customer = findIndividualCustomerByAccountId(accountId,session);
		}else{
			customer = findCompanyCustomerByAccountId(accountId,session);
		}
		return customer;
	}
	
	@Override
	@Transactional
	public Individual findIndividualCustomerByAccountId(int accountId,Session session) {
		if(!session.isOpen()){
			session= getSessionFactory().openSession();
		}
		Individual individual = null;
		try{
			individual = (Individual) session.createQuery("from Individual i where i.account.accountId =:accountId")
					.setParameter("accountId", accountId)
					.uniqueResult();			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}

		return individual;
	}
	
	@Override
	@Transactional
	public Customer findCompanyCustomerByAccountId(int accountId,Session session) {
		if(!session.isOpen()){
			session= getSessionFactory().openSession();
		}
		Company company = null;
		try{
			company = (Company) session.createQuery("from Company c where c.account.accountId =:accountId")
					.setParameter("accountId", accountId)
					.uniqueResult();			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}		

		return company;
	}
	
	/**
	public Account getAuthenticatedUserAccount(int accountId){
		Account account = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("");
			ps.setInt(0, accountId);
			rs = ps.executeQuery();
			if(rs.next()){
				account = new Account();
				account.setAccountId(rs.getInt("accountId"));
				account.setAccountNumber(rs.getString("accountNumber"));
				account.setAccountType(rs.getString("accountType"));
				account.setAccessKey(rs.getString("accessKey"));
				account.setSecretKey(rs.getString("secretKey"));
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);		
		}catch(Exception e){
			logger.warn(e.getMessage(),e);		
		}
		return account;
	}
	**/
}
