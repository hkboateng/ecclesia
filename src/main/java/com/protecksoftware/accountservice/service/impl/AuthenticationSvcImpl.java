package com.protecksoftware.accountservice.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.protecksoftware.accountservice.authentication.AuthenticationResponse;
import com.protecksoftware.accountservice.domain.Account;
import com.protecksoftware.accountservice.domain.AuthenticationUser;
import com.protecksoftware.accountservice.domain.User;
import com.protecksoftware.accountservice.service.AuthenticationService;
import com.protecksoftware.accountservice.utils.AccountServiceUtils;

@Component
public class AuthenticationSvcImpl implements AuthenticationService {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationSvcImpl.class.getName());
	
	private @Value("${sharedSecret}") String secretKey;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	  this.sessionFactory = sessionFactory;
	}
	public SessionFactory getSessionFactory() {
	  return sessionFactory;
	}
	
	@Override
	public AuthenticationUser findCustomerByUsername(String username) {
		AuthenticationUser authenticationUser = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from user where username =?");
			ps.setString(1,username);
			rs = ps.executeQuery();
			if(rs.next()){
				User user = new User();
				user.setAccountId(rs.getInt("accountId"));
				user.setPassword(rs.getString("password"));
				user.setEnabled(rs.getBoolean("enabled"));
				user.setUsername(rs.getString("username"));
				user.setUserId(rs.getInt("userId"));
				user.setAccessKey(rs.getString("accessKey"));
				user.setAccountExpired(rs.getBoolean("accountExpired"));
				user.setAccountLocked(rs.getBoolean("accountLocked"));
				List<String> userPermissions = findEmployeePermissionByUserId(conn,user.getUserId(),user.getAccountId());
				authenticationUser = new AuthenticationUser(user,userPermissions);

			}
		} catch (SQLException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			closeArtifacts(ps,rs);
		}
		return authenticationUser;
	}
	

	private List<String> findEmployeePermissionByUserId(Connection conn, int userId, int accountId) {
		List<String> permissionList = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("select p.* from permissions p JOIN userpermission up ON up.permissionId = p.permissionId where up.userId =? and up.accountId =?");
			ps.setInt(1,userId);
			ps.setInt(2,accountId);
			rs = ps.executeQuery();
			permissionList = new ArrayList<String>();
			while(rs.next()){
				permissionList.add(rs.getString("permission"));
				
			}
		} catch (SQLException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			closeArtifacts(ps,rs);
		}
		return permissionList;
	}
	
	@Override
	public 	AuthenticationResponse validateCustomerCredentials(AuthenticationUser user, String password) {
		AuthenticationResponse response = new AuthenticationResponse();
		if(password == null){
			
			response.setMessage("Password value cannot be null....");
			response.setLoginAttempts(1);
			return response;
		}
		if(user != null){
			String hashPaawd = null;
			try {
				hashPaawd = AccountServiceUtils.getPasswordString(user.getUser().getAccessKey(), password,secretKey);
				boolean success = authenticateUserPassword(hashPaawd,user.getUser().getPassword());
				if(success){
					response.setMessage("Customer login  was successful.");
				}else{
					response.setMessage("Login was not successful, Try again....");
				}
				response.setResult(success);				
			} catch (IOException | GeneralSecurityException e) {
				logger.warn(e.getMessage(),e);		
			}
		}
		return response;
	}
	


	@Override
	public void saveLoginTime(String username, int accountId,Boolean status){
		Connection conn = null;
		PreparedStatement ps = null;
		
		Date date = new Date();
		String dateTime = String.valueOf(date.getTime());
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into login_log(username,status,dateTime,accountId) values (?,?,?,?) ");
			ps.setString(1,username);
			ps.setString(2,status.toString());
			ps.setString(3, dateTime);
			ps.setInt(4,accountId);
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);		
		}finally{
			closeArtifacts(ps);
		}
	}
	
	private void closeArtifacts(PreparedStatement ps){
		try{
			close(ps);
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);			
		}	
	}
	private void closeArtifacts(PreparedStatement ps, ResultSet rs){
		try{
			close(rs);
			close(ps);
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);			
		}	
	}
	private void close(ResultSet rs) throws SQLException{
		if(!rs.isClosed() && rs != null){
			rs.close();
		}
	}
	private void close(Connection conn) throws SQLException{
		if(!conn.isClosed()){
			conn.close();
		}
	}
	private void close(PreparedStatement ps) throws SQLException{
		if(!ps.isClosed()){
			ps.close();
		}
	}
	
	private  BCryptPasswordEncoder passwdencode(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
		return encoder;
	}
	
	private boolean authenticateUserPassword(String password, String hashPassword){
		boolean results = false;
		results = BCrypt.checkpw(password, hashPassword);
		return results;
	}

}
