package com.protecksoftware.accountservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@SpringBootApplication
public class EcclesiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcclesiaApplication.class, args);
	}
	
    @Bean
    public PropertyPlaceholderConfigurer properties() {
        final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
//        ppc.setIgnoreUnresolvablePlaceholders(true);
        ppc.setIgnoreResourceNotFound(true);

        final List<Resource> resourceLst = new ArrayList<Resource>();

        resourceLst.add(new ClassPathResource("properties/accounts.properties"));
        resourceLst.add(new ClassPathResource("properties/authentication.properties"));
        resourceLst.add(new ClassPathResource("properties/messages.properties"));
        ppc.setLocations(resourceLst.toArray(new Resource[]{}));

        return ppc;
    }
    
    
}
