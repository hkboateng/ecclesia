package com.protecksoftware.accountservice.businessobjects;

public interface AccountResponse {

	public boolean getResponseStatus();
	
	public void setResponseStatus(boolean status);
}
