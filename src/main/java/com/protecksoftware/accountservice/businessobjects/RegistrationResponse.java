package com.protecksoftware.accountservice.businessobjects;

import java.util.List;

import com.protecksoftware.accountservice.domain.Company;
import com.protecksoftware.accountservice.domain.Individual;
import com.protecksoftware.accountservice.domain.dto.CompanyDTO;
import com.protecksoftware.accountservice.domain.dto.IndividualDTO;

public class RegistrationResponse implements AccountResponse {
	
	private boolean responseStatus;
	
	private IndividualDTO individual;
	
	private CompanyDTO company;
	
	private String confirmationNumber;
	
	private List<String> registrationErrors;

	private String message;
	
	private int accountId;
	
	public IndividualDTO getIndividual() {
		return individual;
	}

	public void setIndividual(Individual individual) {
		IndividualDTO indv= new IndividualDTO(individual);
		this.individual = indv;
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	public List<String> getRegistrationErrors() {
		return registrationErrors;
	}

	public void setRegistrationErrors(List<String> registrationErrors) {
		this.registrationErrors = registrationErrors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean getResponseStatus() {
		return responseStatus;
	}

	@Override
	public void setResponseStatus(boolean status) {
		this.responseStatus = status;
	}

	public CompanyDTO getCompany() {
		return company;
	}

	public void setCompanyDTO(Company company) {
		CompanyDTO compantDTO = new CompanyDTO(company);
		this.company = compantDTO;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
}
