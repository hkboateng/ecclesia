package com.protecksoftware.accountservice.businessobjects;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;

import com.protecksoftware.accountservice.utils.AccountServiceUtils;

public final class RegistrationRequest {
	private final String accountType;
	
	private final String businessName;
	
	private final String businessName2;
	
	private final String address1;
	
	private final String address2;
	
	private final String city;
	
	private final String region;
	
	private final String phoneNumber;
	
	private final String emailAddress;
	
	private final String contactFirstName;
	
	private final String contactLastName;
	
	private String middlename;
	
	private final String username;
	
	private final String rawPassword;
	
	private final int userRole;
	
	private final  String userKey;

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	private int accountId;

	
	public RegistrationRequest(HttpServletRequest request, String accessKey, String accountType) throws IOException, GeneralSecurityException {
		this.accountType = StringEscapeUtils.escapeJava(accountType);
		this.username = StringEscapeUtils.escapeJava(request.getParameter("txtUsername"));
		String passwd = StringEscapeUtils.escapeJava(request.getParameter("txtPassword"));
		this.userKey = AccountServiceUtils.generatePasswordUniqueKey();
		int role = 0;
		try{
			role = Integer.parseInt(StringEscapeUtils.escapeJava(request.getParameter("roleId")));
		}catch(NumberFormatException e){
			role = 0;
		}
		this.userRole = role;
		this.businessName2 = StringEscapeUtils.escapeJava(request.getParameter("businessName1"));
		this.rawPassword = AccountServiceUtils.generateHashPassword(userKey, passwd, accessKey);
		if(accountType.equalsIgnoreCase("individual")){
			this.middlename = StringEscapeUtils.escapeJava(request.getParameter("middlename"));
			this.address1 = StringEscapeUtils.escapeJava(request.getParameter("address"));
			this.address2 = StringEscapeUtils.escapeJava(request.getParameter("address1"));
			this.city = StringEscapeUtils.escapeJava(request.getParameter("city"));
			this.businessName = "";
			this.region = StringEscapeUtils.escapeJava(request.getParameter("region"));
			this.contactFirstName = StringEscapeUtils.escapeJava(request.getParameter("firstName"));
			this.contactLastName = StringEscapeUtils.escapeJava(request.getParameter("lastName"));
			this.phoneNumber = StringEscapeUtils.escapeJava(request.getParameter("phoneNumber"));
			this.emailAddress = StringEscapeUtils.escapeJava(request.getParameter("emailAddress"));			
		}else{
			this.address1 = StringEscapeUtils.escapeJava(request.getParameter("businessAddress"));
			this.address2 = StringEscapeUtils.escapeJava(request.getParameter("businessAddress1"));
			this.city = StringEscapeUtils.escapeJava(request.getParameter("businessCity"));
			this.businessName = StringEscapeUtils.escapeJava(request.getParameter("businessName"));
			this.region = StringEscapeUtils.escapeJava(request.getParameter("businessRegion"));
			this.contactFirstName = StringEscapeUtils.escapeJava(request.getParameter("contactFirstName"));
			this.contactLastName = StringEscapeUtils.escapeJava(request.getParameter("contactLastName"));
			this.phoneNumber = StringEscapeUtils.escapeJava(request.getParameter("businessPhoneNumber"));
			this.emailAddress = StringEscapeUtils.escapeJava(request.getParameter("businessEmailAddress"));			
		}
		
	}

	public String getAccountType() {
		return accountType;
	}

	public String getUsername() {
		return username;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public int getUserRole() {
		return userRole;
	}


	public String getBusinessName() {
		return businessName;
	}


	public String getAddress1() {
		return address1;
	}


	public String getAddress2() {
		return address2;
	}


	public String getCity() {
		return city;
	}


	public String getRegion() {
		return region;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public String getContactFirstName() {
		return contactFirstName;
	}


	public String getContactLastName() {
		return contactLastName;
	}


	public String getBusinessName2() {
		return businessName2;
	}


	public String getUserKey() {
		return userKey;
	}

	public String getMiddlename() {
		return middlename;
	}
	
	
}
