package com.protecksoftware.accountservice.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class AccountServiceUtils {

	public AccountServiceUtils(){
		
	}
	/**
	 * Get New Instance of ObjectMapper
	 * @return
	 */
	public static ObjectMapper getObectMapperInstance() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		return mapper;
	}
	

	public static String convertToJSON(Object o) throws IOException{
	ObjectMapper mapper = getObectMapperInstance();
		String map = null;
		if(o != null && o instanceof Object){
			map = mapper.writeValueAsString(o);
		}
		return map;
	}
	
	public static <T>  T convertFromJSON(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObectMapperInstance();
		T type = null;
		if(json != null){
			type = mapper.readValue(json,cls);
		}
		
		return type;
	}
	
	public static <T> T convertFromJson(TypeReference<T> r, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObectMapperInstance();
		T t = null;
		if(json != null){
			t = mapper.readValue(json, r);
		}
		return t;
		
	}
	
	public static String convertCheckValue(boolean eInvoice) {
		String isChecked = "";
		if(eInvoice){
			isChecked = "checked";
		}
		return isChecked;
	}

	public static boolean convertYesNoToBoolean(String option) {
		boolean result = false;
		if(option != null && !option.isEmpty()){
			result = option.equalsIgnoreCase("yes") ? true : false;
		}
		return result;
	}
	
	public static String generateHashPassword(String sharedKey,String rawPassword,String accessKey) throws IOException, GeneralSecurityException {
		SecureRandom random = new SecureRandom();
		String passwd = getPasswordString(sharedKey,rawPassword,accessKey);
		return BCrypt.hashpw(passwd, BCrypt.gensalt(10, random));
	}
	
	public static String getPasswordString(String sharedKey,String rawPassword,String accessKey)throws IOException, GeneralSecurityException {
			StringBuilder sbr = new StringBuilder();
			sbr.append(sharedKey).append(rawPassword).append(accessKey).toString();
			
			return sbr.toString();
	}
	
    public static String generatePasswordUniqueKey(){
    	SecureRandom random = new SecureRandom();
    	int length = 128;
    	String alphabet = 
    	        new String("_-*+0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!&?%"); 
    	int n = alphabet.length();

    	String result = new String(); 
    	for (int i=0; i<length; i++) {
    	    result = result + alphabet.charAt(random.nextInt(n));
    	}
    	return result;
    }
    private static String generateStrongCodeHash(String password) throws GeneralSecurityException
    {
    	try{
	        int iterations = 1000;
	        char[] chars = password.toCharArray();
	        byte[] salt = getSalt().getBytes();
	         
	        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
	        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
	        byte[] hash = skf.generateSecret(spec).getEncoded();
	        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
	        
    	}catch( NoSuchAlgorithmException | InvalidKeySpecException e){
			throw e;	        	
        }
    }
    
    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }
     
    private static String getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt.toString();
    }
    
	public static String generateAccountNumber(){

		StringBuilder sbr = new StringBuilder();
		String characters = RandomStringUtils.randomAlphabetic(2).toUpperCase();
		String numbers = RandomStringUtils.randomAlphabetic(8);
		sbr.append(characters)
			.append("-")
			.append(numbers);
			
		return sbr.toString();	
	}
	
	/**
	 * Use for password change and forgot password verification
	 * @return password verification code
	 */
    public static String generateAccountKey(){
    	SecureRandom random = new SecureRandom();
    	int length = 32;
    	String alphabet = 
    	        new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz{}|[]$!"); 
    	int n = alphabet.length();

    	String result = new String(); 
    	for (int i=0; i<length; i++) {
    	    result = result + alphabet.charAt(random.nextInt(n));
    	}
    	return result;
    }
    
	public static String[] convertListToArray(List<String> list){
		if(list == null){
			return null;
		}
		return list.toArray(new String[list.size()]);
	}
	
	public static List<String> convertArraysToList(String[] arr){
		List<String> list = null;
		if(arr != null && arr.length > 0){
			list = Arrays.asList(arr);
		}
		return list;
	}
}
